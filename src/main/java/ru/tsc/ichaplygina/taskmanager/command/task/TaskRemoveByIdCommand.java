package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "remove task by id";

    private final static String DESCRIPTION = "remove task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().removeById(id));
    }

}
