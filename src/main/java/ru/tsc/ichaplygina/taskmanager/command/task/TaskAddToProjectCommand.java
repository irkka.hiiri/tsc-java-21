package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskAddToProjectCommand extends AbstractTaskCommand {

    private final static String NAME = "add task to project";

    private final static String DESCRIPTION = "add task to a project";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        throwExceptionIfNull(getProjectTaskService().addTaskToProject(projectId, taskId));
    }

}
