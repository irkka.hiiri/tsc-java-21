package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "start project by index";

    private final static String DESCRIPTION = "start project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectService().startByIndex(index - 1));
    }

}
