package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    private final static String CMD_NAME = "exit";

    private final static String DESCRIPTION = "quit";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
