package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.model.User;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserShowByLoginCommand extends AbstractUserCommand {

    private static final String NAME = "show user by login";

    private static final String DESCRIPTION = "show user by login";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotPrivilegedUser();
        final String login = readLine(ENTER_LOGIN);
        final User user = getUserService().findByLogin(login);
        showUser(user);
    }

}
