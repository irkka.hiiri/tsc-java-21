package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.APP_HELP_HINT_TEXT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printList;

public class HelpCommand extends AbstractCommand {

    private final static String CMD_NAME = "help";

    private final static String ARG_NAME = "-h";

    private final static String DESCRIPTION = "show this message";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARG_NAME;
    }

    @Override
    public void execute() {
        System.out.println(APP_HELP_HINT_TEXT);
        printList(serviceLocator.getCommandService().getCommandList());
    }

}
