package ru.tsc.ichaplygina.taskmanager.command.task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "remove task by index";

    private final static String DESCRIPTION = "remove task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        throwExceptionIfNull(getTaskService().removeByIndex(index - 1));
    }

}
