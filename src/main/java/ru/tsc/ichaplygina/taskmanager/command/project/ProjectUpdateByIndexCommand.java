package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "update project by index";

    private final static String DESCRIPTION = "update project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getProjectService().updateByIndex(index - 1, name, description));
    }

}
