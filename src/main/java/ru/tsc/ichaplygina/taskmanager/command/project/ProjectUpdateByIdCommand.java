package ru.tsc.ichaplygina.taskmanager.command.project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    private final static String NAME = "update project by id";

    private final static String DESCRIPTION = "update project by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final String id = readLine(ID_INPUT);
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getProjectService().updateById(id, name, description));
    }

}
