package ru.tsc.ichaplygina.taskmanager.command.system;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.APP_VERSION;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public class VersionCommand extends AbstractCommand {

    private final static String CMD_NAME = "version";

    private final static String ARG_NAME = "-v";

    private final static String DESCRIPTION = "show version info";

    @Override
    public String getCommand() {
        return CMD_NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARG_NAME;
    }

    @Override
    public void execute() {
        printLinesWithEmptyLine(APP_VERSION);
    }

}
