package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    private final static String NAME = "show project by id";

    private final static String DESCRIPTION = "show project by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().throwExceptionIfNotAuthorized();
        final String id = readLine(ID_INPUT);
        final Project project = getProjectService().findById(id);
        showProject(project);
    }

}
