package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;

public class Project extends AbstractBusinessEntity implements IWBS {

    public Project() {
    }

    public Project(final String name, final String userId) {
        super(name, userId);
    }

    public Project(final String name, final String description, final String userId) {
        super(name, description, userId);
    }

}
