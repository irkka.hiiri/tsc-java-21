package ru.tsc.ichaplygina.taskmanager.exception.security;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserSelfDeleteNotAllowedException extends AbstractException {

    private static final String MESSAGE = "Cannot remove logged-on user.";

    public UserSelfDeleteNotAllowedException() {
        super(MESSAGE);
    }

}
