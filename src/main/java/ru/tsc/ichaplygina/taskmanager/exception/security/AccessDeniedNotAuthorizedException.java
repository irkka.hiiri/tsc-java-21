package ru.tsc.ichaplygina.taskmanager.exception.security;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class AccessDeniedNotAuthorizedException extends AbstractException {

    private static final String MESSAGE = "Operation unavailable for non-authorized users.";

    public AccessDeniedNotAuthorizedException() {
        super(MESSAGE);
    }

}
