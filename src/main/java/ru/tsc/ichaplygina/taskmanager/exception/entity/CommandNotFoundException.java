package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class CommandNotFoundException extends AbstractException {

    private static final String MESSAGE = "Unknown command or argument: ";

    public CommandNotFoundException(final String command) {
        super(MESSAGE + command);
    }

}
