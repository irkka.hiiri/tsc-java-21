package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class UserNotLoggedInException extends AbstractException {

    private static final String MESSAGE = "User not logged in.";

    public UserNotLoggedInException() {
        super(MESSAGE);
    }

}
