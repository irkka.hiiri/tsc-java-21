package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

public class LogService implements ILogService {

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMAND_FILE = "access.log";

    private static final Logger COMMAND_LOGGER = Logger.getLogger(COMMANDS);

    private static final String ERRORS = "ERRORS";

    private static final String ERROR_FILE = "errors.log";

    private static final Logger ERROR_LOGGER = Logger.getLogger(ERRORS);

    private static final String LOGGER_PROPERTIES_FILE = "logging.properties";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "messages.log";

    private static final Logger MESSAGE_LOGGER = Logger.getLogger(MESSAGES);

    private static final Logger ROOT_LOGGER = Logger.getGlobal();

    private final LogManager logManager = LogManager.getLogManager();

    private Boolean consoleEnabled = true;

    {
        initProperties();
        registerHandlers(COMMAND_LOGGER, COMMAND_FILE);
        registerHandlers(ERROR_LOGGER, ERROR_FILE);
        registerHandlers(MESSAGE_LOGGER, MESSAGES_FILE);
        if (consoleEnabled) {
            COMMAND_LOGGER.addHandler(new ConsoleHandler());
            ERROR_LOGGER.addHandler(new ConsoleHandler());
        }
    }

    public LogService() {
    }

    public LogService(final Boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    @Override
    public void command(final String message) {
        if (message.isEmpty()) return;
        COMMAND_LOGGER.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        ERROR_LOGGER.log(WARNING, e.getMessage(), e);
    }

    public Boolean getConsoleEnabled() {
        return consoleEnabled;
    }

    public void setConsoleEnabled(Boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    @Override
    public void info(final String message) {
        if (message.isEmpty()) return;
        MESSAGE_LOGGER.info(message);
    }

    public void initProperties() {
        try {
            FileInputStream configFile = new FileInputStream(LOGGER_PROPERTIES_FILE);
            logManager.readConfiguration(configFile);
        } catch (IOException e) {
            ROOT_LOGGER.log(SEVERE, "Error! Failed to initialize logging properties: " + e.getMessage(), e);
        }
    }

    public void registerHandlers(final Logger logger, final String fileName) {
        try {
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            ROOT_LOGGER.severe("Error! Failed to register logging file handlers: " + e.getMessage());
        }
    }

}
