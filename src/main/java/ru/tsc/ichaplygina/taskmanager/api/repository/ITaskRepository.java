package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<Task> {

    Task addTaskToProject(String taskId, String projectId);

    Task addTaskToProjectForUser(String userId, String taskId, String projectId);

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId, Comparator<Task> comparator);

    List<Task> findAllByProjectIdForUser(String userId, String projectId);

    List<Task> findAllByProjectIdForUser(String userId, String projectId, Comparator<Task> comparator);

    boolean isNotFoundTaskInProject(String taskId, String projectId);

    void removeAllByProjectId(String projectId);

    Task removeTaskFromProject(String taskId, String projectId);

    Task removeTaskFromProjectForUser(String userId, String taskId, String projectId);

}
