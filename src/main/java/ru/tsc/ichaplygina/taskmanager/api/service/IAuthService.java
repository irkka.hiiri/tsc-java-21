package ru.tsc.ichaplygina.taskmanager.api.service;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

    String getCurrentUserLogin();

    boolean isNoUserLoggedIn();

    boolean isPrivilegedUser();

    void login(String login, String password);

    void logout();

    void throwExceptionIfNotAuthorized();

    void throwExceptionIfNotPrivilegedUser();
}
