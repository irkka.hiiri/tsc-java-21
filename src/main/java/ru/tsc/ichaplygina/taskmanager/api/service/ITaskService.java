package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IBusinessEntityService<Task> {

    Task addTaskToProject(String taskId, String projectId);

    List<Task> findAllByProjectId(String projectId, Comparator<Task> comparator);

    void removeAllByProjectId(String projectId);

    Task removeTaskFromProject(String taskId, String projectId);

}
