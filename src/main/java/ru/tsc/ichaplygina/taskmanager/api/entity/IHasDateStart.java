package ru.tsc.ichaplygina.taskmanager.api.entity;

import java.util.Date;

public interface IHasDateStart {

    Date getDateStart();

    void setDateStart(Date date);

}
